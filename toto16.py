import copy as cp
f = open('input16')
s = f.readlines()
f.close()

s = [x.rstrip() for x in s]

eq=['children','samoyeds','akitas','vizslas','cars','perfumes']
lt=['pomeranians','goldfish']
gt=['cats','trees']
realAunt='children:3,cats:7,samoyeds:2,pomeranians:3,akitas:0,vizslas:0,goldfish:5,trees:3,cars:2,perfumes:1'.split(',')
realAunt = [x.split(':') for x in realAunt]
dictRealAunt = {x[0]:int(x[1]) for x in realAunt}
for i in range(500):
    same = True
    e = s[i]
    e = e.split(',')
    e = [x.split(':') for x in e]
    dictAct = {x[0]:int(x[1]) for x in e}
    print dictAct
    print dictRealAunt
    for x in dictAct.keys():
        if x in eq:
            print 'eq :', x
            same = same and (dictRealAunt[x] == dictAct[x])
        if x in lt:
            print 'lt :', x
            same = same and (dictRealAunt[x] > dictAct[x])
        if x in gt:
            print 'gt :',x
            same = same and (dictRealAunt[x] < dictAct[x])
    if same:
        print i+1
        break

