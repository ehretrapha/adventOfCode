f = open('input2','r')
t = f.readlines()
f.close()

t = [x.split('x') for x in t]
t = [[int(x.rstrip()) for x in l] for l in t]

papier = 0
ruban = 0
for l,w,h in t:
    papier += 2*l*w+2*w*h+2*h*l+min(l*w,w*h,h*l)
    ruban += l*w*h + min(2*(l+w),2*(w+h),2*(h+l))

print 'papier :', papier
print 'ruban :', ruban
