import itertools
import numpy as np

def sumHappy(happyValues,disposition):
    total = 0
    for i in (range(-1,len(disposition)-1)):
        total += happyValues[disposition[i]+disposition[i-1]]
        total += happyValues[disposition[i]+disposition[i+1]]
    return total

f = open('input13bis')
t = f.readlines()
f.close()
t = [x.split() for x in t]
s = {}
for e in t:
    if e[2] == 'lose':
        s[e[0]+e[-1][:-1]]=-int(e[3])
    else:
        s[e[0]+e[-1][:-1]]=int(e[3])

names = set([x[0] for x in t])
value = -np.inf
for e in itertools.permutations(names,len(names)):
    value = max(value,sumHappy(s,e))
print value

