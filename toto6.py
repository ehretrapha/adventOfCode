import numpy as np

f = open('input6','r')
t = f.readlines()
f.close()

lights = np.zeros((1000,1000))
cmpt1 = 0
cmpt2 = 0
for l in t:
    action = l.split()[0:2]
    if action == ['turn','on']:
        my_action = 'on'        
        begin_tuple = tuple(map(int,l.split()[2].split(',')))
        end_tuple = tuple(map(int,l.split()[4].split(',')))
        lights[begin_tuple[0]:end_tuple[0]+1,begin_tuple[1]:end_tuple[1]+1] += 1

    elif action == ['turn','off']:
        cmpt1 +=1
        my_action = 'off'
        begin_tuple = tuple(map(int,l.split()[2].split(',')))
        end_tuple = tuple(map(int,l.split()[4].split(',')))
        lights[begin_tuple[0]:end_tuple[0]+1,begin_tuple[1]:end_tuple[1]+1] -= 1
        under_zeros = lights < 0
        lights[under_zeros] = 0
    else:
        my_action = 'toggle'
        begin_tuple = tuple(map(int,l.split()[1].split(',')))
        end_tuple = tuple(map(int,l.split()[3].split(',')))
        lights[begin_tuple[0]:end_tuple[0]+1,begin_tuple[1]:end_tuple[1]+1] += 2

ml = [sum(list(x)) for x in lights]
print sum(ml)
