import copy as cp
import numpy as np

f = open('input9','r')
t = f.readlines()
f.close()

t = [x.split() for x in t]
t = [[x[0],x[2],x[4]] for x in t]
listeDistances = t

villes = set([x[0] for x in t] + [x[1] for x in t])

def distance(a,b,listeDistances):
    if not(all([a,b])):
        return 0
    for d in listeDistances:
        if (a,b) == (d[0],d[1]) or (a,b) == (d[1],d[0]):
            return int(d[2])


def nouvelleVille(villeCourante,parcours,villes,listeDistances):
    listeCouts = []
     
    if len(parcours) == 7:
        return [distance(villeCourante, [v for v in villes if not(v in parcours)][0], listeDistances)]
    for v in [v for v in villes if not(v in parcours)]:
        coutVoyageVersV = distance(villeCourante,v, listeDistances)
        listeCouts += map(lambda x: coutVoyageVersV + x, nouvelleVille(v,parcours+[v],villes, listeDistances))
    return listeCouts

listeCouts = nouvelleVille('',[],villes,listeDistances)
print len(listeCouts)
print max(listeCouts)

