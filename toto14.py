def incrementMax(points,distances):
    maxim = 0
    liste = []
    for i in range(len(distances)):
        if distances[i] > maxim:
            liste = [i]
            maxim = distances[i]
        elif distances[i] == maxim:
            liste.append(i)
    for i in liste:
        points[i] += 1
    return points



f = open('input14','r')
t = f.readlines()
f.close
t = [x.rstrip() for x in t]
t = [x.split() for x in t]
s = [(int(x[3]),int(x[6]),int(x[13])) for x in t]
nbsecondes = 2503 

### Exemple given
#s = [(14,10,127),(16,11,162)]
#nbsecondes = 1000 

distances = [0 for _ in s]
points = [0 for _ in s]
for i in range(nbsecondes):
    compteur = 0
    for x in s:
        if i%(x[1]+x[2]) < (x[1]):
            distances[compteur] += x[0]
        compteur += 1
    points = incrementMax(points,distances) 
print max(points)
