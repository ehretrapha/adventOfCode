f = open('input17')
s = f.readlines()
f.close()

qte = 150
cont = sorted([int(x.rstrip()) for x in s])


def addc(nbobjets,cont,qte):
    if not(qte):
        return [nbobjets]
    chemins = []
    for e in cont:
        cont = cont[1:]
        if qte >= e:
            chemins += addc(nbobjets+1,cont,qte-e)
    return chemins

res = addc(0,cont,qte)
print res.count(min(res))


