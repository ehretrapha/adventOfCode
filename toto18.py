import copy as cp

f = open('input18')
s = f.readlines()
f.close()

s = [list(x.rstrip()) for x in s]
tr = ['.' for _ in range(100)]
s.insert(0,tr)
s.append(tr)
s = [list('.') + x + list('.') for x in s]

alwaysOn = [(1,1),(1,100),(100,1),(100,100)]
for i,j in alwaysOn:
    s[i][j] = '#'

for _ in range(100):
    scopy = cp.deepcopy(s)
    for index1 in range(1,101):
        for index2 in range(1,101):
            if not((index1,index2) in alwaysOn):
                compteur = 0
                for i in [-1,0,1]:
                    for j in [-1,0,1]:
                        if ((i,j) != (0,0) and s[index1+i][index2+j] == '#'):
                            compteur += 1
                if (not(compteur in [2,3])):
                    scopy[index1][index2] = '.'
                if compteur == 3:
                    scopy[index1][index2] = '#'
    s = scopy

print sum([x.count('#') for x in s[1:-1]])
            

