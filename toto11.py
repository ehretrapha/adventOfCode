s = raw_input('Enter your old password\n')

def isConsec(s):
    prev = 0
    nbConsec = 0
    for e in s:
        if ord(e) == (prev+1):
            nbConsec += 1
            if nbConsec > 1:
                return True
        else:
            nbConsec = 0
        prev = ord(e)
    return False

def haveForbidden(s):
    evilChars = ['i','o','l']
    return reduce(lambda x,y: x or y,[c in s for c in evilChars])

def haveTwoPairs(s):
    pair = -2
    for i in range(len(s)-1):
        if s[i] == s[i+1]:
            if pair >= 0:
                if pair+1 != i :
                    return True
            else:
                pair = i
    return False
            
def isGood(s):
    return (isConsec(s) and not(haveForbidden(s)) and haveTwoPairs(s))

def increment(s):
    res = []
    reste = 1
    for i in range(len(s))[::-1]:
        c = ord(s[i])+1-97
        if reste == 1:
            if c%26 == 0:
                res.append(chr(97))
                if i == 0:
                    res = ['a'] + res
            else:
                res.append(chr(c+97))
                reste = 0
        else:
            res.append(s[:i+1][::-1])
            it = i
            break
    return(reduce(lambda a,b:a+b,[c for c in res])[::-1])
            
            
it = 0
while True:
    print s
    if isGood(s):
        it +=1
        if it == 2:
            break
    s = increment(s)
print s

#print increment(s)
