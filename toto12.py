import sys
sys.setrecursionlimit(100000)

def getElements(data):
    total = 0
    if isinstance(data,list):
        for e in data:
            total += getElements(e)
    elif isinstance(data,dict):
        if 'red' in data.values():
            return 0
        else:
            for v in data.values():
                total += getElements(v)
    else:
        if isinstance(data,int):
            return data
        else:
            return 0
    return total
        

import json
with open('input12bis') as json_data:
    data = json.load(json_data)
    somme = getElements(data)
    print somme
    
