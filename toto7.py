f = open('input7','r')
t = f.readlines()
f.close()

class wire:
    def __init__(self, *args):
        if len(args) == 1:
            self.name = args[0].split()[-1].rstrip()
            self.father1,self.father2,self.operation = self.parseInputString(args[0][:-6].split())
        elif len(args) == 4:
            self.name = args[0]
            self.father1 = args[1]
            self.father2 = args[2]
            self.operation = args[3]

    def parseInputString(self,inputString):
        if len(inputString) == 1:
            if inputString[0].isdigit():
                return (inputString[0],'','init')
            else:
                return (inputString[0],'','->') 
        elif inputString[0] == 'NOT':
            return (inputString[1],'','NOT')
        else:
            return (inputString[0],inputString[2],inputString[1])

def getWireByName(wireList,name):
    for w in wireList:
        if w.name == name:
            return w
    return -1



def getWireListByOperation(wireList,operation):
    return [w for w in wireList if w.operation == operation]

wires = [wire(s) for s in t]
a = getWireByName(wires,'v')

connectedWires = [wire('1','1','','init')]
connectedWires += getWireListByOperation(wires,'init')
wires = [w for w in wires if w.operation != 'init']
while wires:
    for i in range(len(wires)):
        w = wires[i]
        father1 = getWireByName(connectedWires,w.father1)
        father2 = getWireByName(connectedWires,w.father2)
        if father1 != -1:
            if w.operation == '->':
                connectedWires.append(wire(w.name,father1.father1,'','init'))
                wires.pop(i)
                break
            if w.operation == 'NOT':
                connectedWires.append(wire(w.name,~(father1.father1),'','init'))
                wires.pop(i)
                break
            if w.operation == 'LSHIFT':
                connectedWires.append(wire(w.name,(int(father1.father1)<<int(w.father2)),'','init'))
                wires.pop(i)
                break
            if w.operation == 'RSHIFT':
                connectedWires.append(wire(w.name,(int(father1.father1)>>int(w.father2)),'','init'))
                wires.pop(i)
                break
            if w.operation == 'AND' and father2 != -1:
                connectedWires.append(wire(w.name,int(father1.father1) & int(father2.father1),'','init'))
                wires.pop(i)
                break
            if w.operation == 'OR' and father2 != -1:
                connectedWires.append(wire(w.name,int(father1.father1) | int(father2.father1),'','init'))
                wires.pop(i)
                break

print getWireByName(connectedWires,'a').father1
