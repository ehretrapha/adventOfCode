f = open('input15')
s = f.readlines()
f.close()

s = [x.rstrip().split() for x in s]
values = [(int(x[2]), int(x[4]), int(x[6]), int(x[8])) for x in s]
calories = [int(x[10]) for x in s ]
score = 0
for i1 in range(1,101):
    for i2 in range(1,101-i1):
        for i3 in range(1,101-i1-i2):
            i4 = 100-i1-i2-i3
            i = [i1,i2,i3,i4]
            if sum(map(lambda x,y:x*y,i,calories)) == 500:
                sum0 = max(0,sum(map(lambda x,y:x*y,i,[x[0] for x in values])))
                sum1 = max(0,sum(map(lambda x,y:x*y,i,[x[1] for x in values])))
                sum2 = max(0,sum(map(lambda x,y:x*y,i,[x[2] for x in values])))
                sum3 = max(0,sum(map(lambda x,y:x*y,i,[x[3] for x in values])))
                score = max(score,max(0,sum0)*max(0,sum1)*max(0,sum2)*max(0,sum3))
print score




